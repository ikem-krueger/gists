// Here's another method (it currently randomly sets MAC address, but
// would be easy to modify.  This uses va_arg to get the correct request.


// MAC spoofer with ioctl hook(v0.3)
// (c) 2006 [*C] team
//
// compile:
// gcc -g -Wall -fPIC -c t_ioctl_hook.c
// link:
// gcc -ldl -shared t_ioctl_hook.o -o t_ioctl_hook.so
// use:
// export LD_PRELOAD="/path/to/t_ioctl_hook.so"

#include <stdlib.h>
#include <time.h>
#include <net/if.h>
#include <stdarg.h>
#define __USE_GNU
#include <dlfcn.h>
int (*orig_ioctl) (int fd,unsigned long int request,...); // ptr to the original ioctl() ...

void __attribute__ ((constructor)) my_init(void) { // ... we grab it here in init
       orig_ioctl = (int (*)(int fd,unsigned long int request ,...)) dlsym(RTLD_NEXT, "ioctl");
}
//fake ioctl() here,0x8927 used instead of a name because there is no include for ioctl() here
int ioctl(int fd,unsigned long int request,...) {
       struct ifreq *arg;
       int comeback;
       int i;
       time_t *t;
       va_list arglist;
       
       va_start(arglist, request);
       arg = va_arg(arglist, void *);
       va_end(arglist);
       comeback=orig_ioctl(fd,request,arg); // call original ioct() first
       if(request==0x8927) { //SIOCGIFHWADDR  - get hw addr request
               t=malloc(20);
               srand(time(t));
               for (i=0; i < 6; i++)
                       arg[0].ifr_ifru.ifru_hwaddr.sa_data[i]=1+(int) (250.0*rand()/(RAND_MAX+1.0)); //rand
               free(t);
       } // then set fake hw addr
       return(comeback); //return with original value
}


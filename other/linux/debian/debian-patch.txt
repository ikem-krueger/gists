How to patch and build a debian package
---------------------------------------

First make sure you have the deb-src repositories in sources.list:

  $ grep deb-src /etc/apt/sources.list
  deb-src http://security.ubuntu.com/ubuntu precise-security main restricted
  deb-src http://security.ubuntu.com/ubuntu precise-security universe
  deb-src http://security.ubuntu.com/ubuntu precise-security multiverse
  deb-src http://archive.canonical.com/ubuntu precise partner
  deb-src http://extras.ubuntu.com/ubuntu precise main
  deb-src http://br.archive.ubuntu.com/ubuntu/ubuntu/ precise main restricted
  deb-src http://br.archive.ubuntu.com/ubuntu/ubuntu/ precise-updates main restricted
  deb-src http://br.archive.ubuntu.com/ubuntu/ubuntu/ precise universe
  deb-src http://br.archive.ubuntu.com/ubuntu/ubuntu/ precise-updates universe
  deb-src http://br.archive.ubuntu.com/ubuntu/ubuntu/ precise multiverse
  deb-src http://br.archive.ubuntu.com/ubuntu/ubuntu/ precise-updates multiverse
  deb-src http://br.archive.ubuntu.com/ubuntu/ precise-security main restricted
  deb-src http://br.archive.ubuntu.com/ubuntu/ precise-security universe
  deb-src http://br.archive.ubuntu.com/ubuntu/ precise-security multiverse

If not, add these lines to /etc/apt/sources.list and run

  # apt-get update

After that you can get the wireshark sources. The files will be downloaded
to the current directory.

  $ apt-get source wireshark

This will download the wireshark source, unpack the tarball and copy the
"debian" diretory into it:

  $ cd wireshark-1.6.7

All further steps assume you are in this directory.
 
In this directory, you can find the "debian" subdirectory, which conntains
the build and installation instructions for a debian package. It also
contains a "debian/patches" directory which contains the patches that
debian and/or ubuntu apply to the original sources.

  $ ls debian/patches
  00list                                     06_release-version.patch
  01_idl2deb.patch                           07_use-theme-icon.patch
  02_asn2deb.patch                           08_wireshark-desktop-menu.patch
  03_preferences.dpatch                      09_idl2wrs.patch
  03_preferences.patch                       16_licence_about_location.patch
  04_asn2wrs_ply.patch                       series
  05_note-README-when-running-as-root.patch

We will create a patch that fits in this hierarchy using the tool called
"quilt":

  # apt-get install quilt

The quilt patch system manages a stack of patches to the original source.
Since we want to add a new patch, we must first apply all patches contained
in the package.

  $ export QUILT_PATCHES=debian/patches
  $ quilt push -a
  File series fully applied, ends at patch 16_licence_about_location.patch

Now we can create our patch. We'll prefix with with "17_" since the last
patch in debian/patches is prefixed with "16_".

  $ quilt new 17_readme.patch
  Patch 17_readme.patch is now on top
  $ quilt add README          # do this for every file you edit
  File README added to patch 17_readme.patch
  $ echo 'This wireshark has XIA support' >> README
  $ quilt refresh
  Refreshed patch 17_readme.patch
  $ quilt pop -a

That last command will undo all the applied patches, including yours, and
leave the sources clean. You can see that your patch was added to the patch
system:

  $ ls debian/patches 
  00list   06_release-version.patch
  01_idl2deb.patch                           07_use-theme-icon.patch
  02_asn2deb.patch                           08_wireshark-desktop-menu.patch
  03_preferences.dpatch                      09_idl2wrs.patch
  03_preferences.patch                       16_licence_about_location.patch
  04_asn2wrs_ply.patch                       17_readme.patch
  05_note-README-when-running-as-root.patch  series

  $ cat debian/patches/series
  01_idl2deb.patch
  02_asn2deb.patch
  03_preferences.patch
  04_asn2wrs_ply.patch
  05_note-README-when-running-as-root.patch
  06_release-version.patch
  07_use-theme-icon.patch
  08_wireshark-desktop-menu.patch
  09_idl2wrs.patch
  16_licence_about_location.patch
  17_readme.patch

You can now build the package, but first you need to install its build dependencies.

  # apt-get build-dep wireshark

If you decide to rename the package (say, to "wireshark-xia"), you must do so
in two places: first, add a new entry to the debian/changelog file (be sure
to respect the indentation; it must be exactly the same as in the other
entries). In your entry, instead of using "wireshark" as the package name
in the first line of the changelog entry, change it to "wireshark-xia". The
changelog can be edited by using the dch utility. To add a new entry to the
changelog, issue:

  $ dch -i 

Here you can rename the package, change the version, add in your name/email
address, and provide a synopsis of changes made.

Second, edit the debian/control file and change the "Source:" header to
"wireshark-xia" and change the "Package:" headers to add the new name. For
example, change "Package: wireshark-common" to "Package: wireshark-xia-common".
Note that some libraries are also built from this package (eg. libwireshark1,
libwiretap1). If you rename those too, be sure to replace the old name globally
in the control file, because some of the packages depend on those libraries,
and the name listed in the "Depends:" header must match with the one declared
in the "Package:" header. If you do change the package naming scheme, you will
have to change the name of the original tarball in order to execute the debuild
step.

Finally, you can now generate the debian package:

  # apt-get install devscripts
  $ debuild -uc -us

When this command finishes, you'll find the *.deb files in the parent
directory. They can be installed normally with "dpkg -i <package>.deb".

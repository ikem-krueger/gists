Unfortunately, `apt-config dump` doesn't list the dpkg options you were looking for. `man dpkg` helped me find this command, `--force-help`, which is more specific to the particular dpkg `force` options you were interested in, but not a comprehensive list of options:

    $ dpkg --force-help
    dpkg forcing options - control behaviour when problems found:
      warn but continue:  --force-<thing>,<thing>,...
      stop with error:    --refuse-<thing>,<thing>,... | --no-force-<thing>,...
     Forcing things:
      [!] all                Set all force options
      [*] downgrade          Replace a package with a lower version
          configure-any      Configure any package which may help this one
          hold               Process incidental packages even when on hold
          not-root           Try to (de)install things even when not root
          bad-path           PATH is missing important programs, problems likely
          bad-verify         Install a package even if it fails authenticity check
          bad-version        Process even packages with wrong versions
          overwrite          Overwrite a file from one package with another
          overwrite-diverted Overwrite a diverted file with an undiverted version
      [!] overwrite-dir      Overwrite one package's directory with another's file
      [!] unsafe-io          Do not perform safe I/O operations when unpacking
      [!] confnew            Always use the new config files, don't prompt
      [!] confold            Always use the old config files, don't prompt
      [!] confdef            Use the default option for new config files if one
                             is available, don't prompt. If no default can be found,
                             you will be prompted unless one of the confold or
                             confnew options is also given
      [!] confmiss           Always install missing config files
      [!] confask            Offer to replace config files with no new versions
      [!] architecture       Process even packages with wrong or no architecture
      [!] breaks             Install even if it would break another package
      [!] conflicts          Allow installation of conflicting packages
      [!] depends            Turn all dependency problems into warnings
      [!] depends-version    Turn dependency version problems into warnings
      [!] remove-reinstreq   Remove packages which require installation
      [!] remove-essential   Remove an essential package
    
    WARNING - use of options marked [!] can seriously damage your installation.
    Forcing options marked [*] are enabled by default.



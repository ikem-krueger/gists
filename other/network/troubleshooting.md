| Layer | About | Testing Tool |
| - | - | - |
| Physical Layer | Signal level, connectors and cabling | Cabletester |
| Data Link Layer | MAC | "arp" |
| Network Layer | IP, IPX | "ping", "traceroute" |
| Transport Layer | TCP, UDP | - |
| Session Layer | DNS | "nslookup", "dig" |
| Presentation Layer | ASCII, JPEG, CSV | - |
| Application Layer | Telnet | - |

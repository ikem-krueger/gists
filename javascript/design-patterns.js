// ---------------
// Factory Pattern
// ---------------

// factory function to create car objects
function createCar(make, model, year) {
    var o = new Object();

    o.make   = make;
    o.model  = model;
    o.year   = year;
    o.sayCar = function() {
        alert('I have a ' + this.year + ' ' + this.make + ' ' + this.model + '.');
    };

    return o;
}

// create 2 car objects for John and Jane
var johnCar = createCar('Ford', 'F150', '2011'),
    janeCar = createCar('Audi', 'A4', '2007');

// call method on Jane's car
janeCar.sayCar();




// -------------------
// Constructor Pattern
// -------------------

// constructor function to create car objects
function Car(make, model, year) {
    this.make   = make;
    this.model  = model;
    this.year   = year;
    this.sayCar = function() {
        alert('I have a ' + this.year + ' ' + this.make + ' ' + this.model + '.');
    };
}

// create 2 car objects for John and Jane
var johnCar = new Car('Ford', 'F150', '2011'),
    janeCar = new Car('Audi', 'A4', '2007');

// call method on Jane's car
janeCar.sayCar();




// -----------------------------------------
// Combination Constructor/Prototype Pattern
// -----------------------------------------

// constructor function to create car objects
function Car(make, model, year) {
    this.make   = make;
    this.model  = model;
    this.year   = year;
}

// constructor prototype to share properties and methods
Car.prototype.sayCar = function() {
    alert('I have a ' + this.year + ' ' + this.make + ' ' + this.model + '.');    
};

// create 2 car objects for John and Jane
var johnCar = new Car('Ford', 'F150', '2011'),
    janeCar = new Car('Audi', 'A4', '2007');

// call method on Jane's car
janeCar.sayCar();




// -------------------------
// Dynamic Prototype Pattern
// -------------------------

// constructor function to create car objects
function Car(make, model, year) {
    this.make   = make;
    this.model  = model;
    this.year   = year;

    // constructor prototype to share properties and methods
    if ( typeof this.sayCar !== "function" ) {
        Car.prototype.sayCar = function() {
            alert('I have a ' + this.year + ' ' + this.make + ' ' + this.model + '.');    
        }        
    }
}


// create 2 car objects for John and Jane
var johnCar = new Car('Ford', 'F150', '2011'),
    janeCar = new Car('Audi', 'A4', '2007');

// call method on Jane's car
janeCar.sayCar();




// ------------
// OLOO Pattern
// ------------

// constructor object to create car objects
var Car = {
    init: function(make, model, year) {
        this.make   = make;
        this.model  = model;
        this.year   = year;        
    },
    sayCar: function() {
        alert('I have a ' + this.year + ' ' + this.make + ' ' + this.model + '.');    
    }
};


// create 2 car objects for John and Jane
var johnCar = Object.create(Car),
    janeCar = Object.create(Car);

// call init method on John and Jane
johnCar.init('Ford', 'F150', '2011');
janeCar.init('Audi', 'A4', '2007');

// call method on Jane's car
janeCar.sayCar();




// -----------------------------
// Parasitic Constructor Pattern
// -----------------------------

// constructor function to create car objects
function Car(make, model, year) {
    var o = new Object();

    o.make   = make;
    o.model  = model;
    o.year   = year;
    o.sayCar = function() {
        alert('I have a ' + this.year + ' ' + this.make + ' ' + this.model + '.');
    };

    return o;
}


// create 2 car objects for John and Jane
var johnCar = new Car('Ford', 'F150', '2011'),
    janeCar = new Car('Audi', 'A4', '2007');

// call method on Jane's car
janeCar.sayCar();




// -----------------------------
// Durable Constructor Pattern
// -----------------------------

// constructor function to create car objects
function Car(make, model, year) {
    var o = new Object();

    // private variable
    var condition = 'used';

    // public method
    o.sayCar = function() {
        alert('I have a ' + condition + ' ' + year + ' ' + make + ' ' + model + '.');
    };

    return o;
}


// create 2 car objects for John and Jane
var johnCar = Car('Ford', 'F150', '2011'),
    janeCar = Car('Audi', 'A4', '2007');

// call method on Jane's car
janeCar.sayCar();

# Clear notification badges
defaults write com.apple.systempreferences AttentionPrefBundleIDs 0

# Run Script on Login
defaults write com.apple.loginwindow LoginHook /path/to/script

# Disable AppNap
defaults write NSGlobalDomain NSAppSleepDisabled -bool YES

# Safari - Show debug menu
defaults write com.apple.Safari IncludeInternalDebugMenu 1

# Safari - Disable Auto-Play Videos
defaults write com.apple.Safari WebKitMediaPlaybackAllowsInline 0
defaults write com.apple.Safari com.apple.Safari.ContentPageGroupIdentifier.WebKit2AllowsInlineMediaPlayback 0

# Safari - Disable Full Page Accelerated Drawing
defaults write com.apple.safari WebKitAcceleratedDrawingEnabled 0
defaults write com.apple.safari com.apple.Safari.ContentPageGroupIdentifier.WebKit2AcceleratedDrawingEnabled 0

# Safari - Disable Canvas Accelerated Drawing
defaults write com.apple.safari WebKitCanvasUsesAcceleratedDrawing 0
defaults write com.apple.safari com.apple.Safari.ContentPageGroupIdentifier.WebKit2CanvasUsesAcceleratedDrawing 0

# Improving Xcode Simulator Performance
defaults write com.apple.CoreSimulator.IndigoFramebufferServices FramebufferRendererHint 3 # 0=auto, 1=Metal, 2=OpenCL, 3=OpenGL

# Automatically hide and show the Menu bar
defaults write NSGlobalDomain _HIHideMenuBar -bool true
killall Finder

# Disable the “Are you sure you want to open this application?” dialog
defaults write com.apple.LaunchServices LSQuarantine -bool false

# Check for software updates daily, not just once per week
defaults write com.apple.SoftwareUpdate ScheduleFrequency -int 1

# Disable local Time Machine snapshots
sudo tmutil disablelocal

# Disable hibernation (speeds up entering sleep mode)
sudo pmset -a hibernatemode 0

# Save screenshots to the pictures
defaults write com.apple.screencapture location -string "${HOME}/Pictures/Screenshots"
killall SystemUIServer

# Save screenshots in PNG format (other options: BMP, GIF, JPG, PDF, TIFF)
defaults write com.apple.screencapture type -string "png"
killall SystemUIServer

# Enable subpixel font rendering on non-Apple LCDs
defaults -currentHost write -globalDomain AppleFontSmoothing -int 2
defaults write NSGlobalDomain AppleFontSmoothing -int 2

# When performing a search, search the current folder by default
defaults write com.apple.finder FXDefaultSearchScope -string "SCcf"

# Disable the warning when changing a file extension
defaults write com.apple.finder FXEnableExtensionChangeWarning -bool false

# Speed up Mission Control animations
defaults write com.apple.dock expose-animation-duration -float 0.1

# Disable Dashboard
defaults write com.apple.dashboard mcx-disabled -bool true

# Don’t show Dashboard as a Space
defaults write com.apple.dock dashboard-in-overlay -bool true

# Don’t automatically rearrange Spaces based on most recent use
defaults write com.apple.dock mru-spaces -bool false

# Remove the auto-hiding Dock delay
defaults write com.apple.dock autohide-delay -float 0
killall Dock

defaults write com.apple.dock autohide-time-modifier -float 0.8
killall Dock

# Automatically hide and show the Dock
defaults write com.apple.dock autohide -bool true
killall Dock

# Prevent Time Machine from prompting to use new hard drives as backup volume
defaults write com.apple.TimeMachine DoNotOfferNewDisksForBackup -bool true

# Disable local Time Machine backups
hash tmutil &> /dev/null && sudo tmutil disablelocal

# Use plain text mode for new TextEdit documents
defaults write com.apple.TextEdit RichText -int 0

# Open and save files as UTF-8 in TextEdit
defaults write com.apple.TextEdit PlainTextEncoding -int 4
defaults write com.apple.TextEdit PlainTextEncodingForWrite -int 4

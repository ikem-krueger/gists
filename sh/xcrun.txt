xcrun simctl list
xcrun simctl list|grep Booted # show booted xcode simulators
xcrun simctl list|grep Shutdown
xcrun simctl create "iPhone SE" com.apple.CoreSimulator.SimDeviceType.iPhone-SE com.apple.CoreSimulator.SimRuntime.iOS-12-0
xcrun simctl delete 760F6ECE-2206-4DFB-8D31-BC77581FAF44
xcrun simctl shutdown 760F6ECE-2206-4DFB-8D31-BC77581FAF44
xcrun simctl boot 760F6ECE-2206-4DFB-8D31-BC77581FAF44
xcrun simctl erase 760F6ECE-2206-4DFB-8D31-BC77581FAF44 # factory reset device
xcrun simctl install 760F6ECE-2206-4DFB-8D31-BC77581FAF44 Fancy.app # install app on the xcode simulator
xcrun simctl install booted TicTacToe.app # install app on all booted xcode simulators
defaults read TicTacToe.app/Info.plist|grep BundleIdentifier # get the apps bundle identifier
xcrun simctl launch booted com.tutorial.TicTacToe # launch app with the bundle identifier
xcrun simctl terminate booted com.tutorial.TicTacToe
xcrun simctl uninstall booted com.tutorial.TicTacToe
xcrun simctl addmedia booted ~/Desktop/shashi.png
xcrun simctl addmedia booted ~/Desktop/simctl_list.gif
xcrun simctl io booted screenshot screen.png # make a screenshot of the screen
xcrun simctl io booted recordVideo news.mov # record a video of the screen
xcrun simctl spawn booted log stream --level=debug
xcrun simctl spawn booted log stream --predicate 'processImagePath endswith "CLI"'
xcrun simctl spawn booted log stream --predicate 'eventMessage contains "error" and messageType == info'
xcrun simctl spawn booted log collect
cd `xcrun simctl getenv booted SIMULATOR_SHARED_RESOURCES_DIRECTORY`

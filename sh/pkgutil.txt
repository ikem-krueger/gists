pkgutil --pkgs # list all installed packages
pkgutil --files the-package-name.pkg # list installed files
pkgutil --pkg-info the-package-name.pkg # check the location
pkgutil --only-files --files the-package-name.pkg|tr '\n' '\0'|xargs -n 1 -0 rm -i
pkgutil --only-dirs --files the-package-name.pkg|tr '\n' '\0'|xargs -n 1 -0 rm -ir
pkgutil --forget the-package-name.pkg # remove the receipt

extension String {
    public var randomLetter: Character {
        return self[self.index(self.startIndex, offsetBy: arc4random_uniform(UInt32(self.count)))]
    }
}

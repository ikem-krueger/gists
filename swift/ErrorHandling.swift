/// Use a do - catch block and handle the error (recommended)
do {
   let json = try JSON(data: data)
   print(json)
} catch {
   print(error)
   // or display a dialog
}

/// Ignore the error and optional bind the result (useful if the error does not matter)
if let json = try? JSON(data: data) {
   print(json)
}

/// Force unwrap the result
let json = try! JSON(data: data)

print(json)

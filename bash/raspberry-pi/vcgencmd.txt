vcgencmd commands
vcgencmd version # show the firmware version
vcgencmd get_config int # get config.txt
vcgencmd get_mem arm # show how much memory is used by the cpu
vcgencmd get_mem gpu # show how much memory is used by the gpu
vcgencmd codec_enabled MPG2 # show if the specified codec is enabled
vcgencmd codec_enabled WVC1
vcgencmd codec_enabled H264
vcgencmd codec_enabled MPG4
vcgencmd codec_enabled MJPG
vcgencmd codec_enabled WMV9
vcgencmd measure_temp
vcgencmd measure_volts
vcgencmd measure_clock arm
vcgencmd get_throttled
vcgencmd arbiter set arm_uc 12 0
vcgencmd otp_dump # display the contents of the otp (one time programmable) memory
vcgencmd display_power 0
vcgencmd display_power 1

irm https://get.activated.win | iex
$env:PSModulePath -split ';' # show default module locations
(Get-Help about_PSReadLine)[0].ToString() # show the content of individual list items 
Get-Date -Format "FileDateTimeUniversal"
[datetime]::Now.ToUniversalTime().ToString("MMddyy HH:mm:ss") # get date and time
1..1024 | % {write-host ((new-object Net.Sockets.TcpClient).Connect("192.168.1.5",$_)) "Port $_ ist offen"} 2>$null
1..65335 | % -ThrottleLimit 500 -Parallel {write-host ((new-object Net.Sockets.TcpClient).Connect("192.168.1.5",$_)) "Port $_ is open!"} 2>$null
Get-Content mytext.txt | Set-Content -Encoding utf8 mytext_utf8.txt # convert utf-8 to utf-16
Get-Acl -Path 'C:\Share\Veteran' | Set-Acl -Path 'E:\PublicDOCS' # copy ntfs permissions from one folder to another
Get-FileHash -Algorithm SHA256 File
Get-EventLog -LogName Application -Source "microsoft-windows-defrag" | sort timegenerated | fl timegenerated, message
Get-AppxPackage Microsoft.Windows.StartMenuExperienceHost | Foreach {Add-AppxPackage -DisableDevelopmentMode -Register "$($_.InstallLocation)\AppXManifest.xml"}
Get-AppxPackage Microsoft.Windows.ShellExperienceHost | Foreach {Add-AppxPackage -DisableDevelopmentMode -Register "$($_.InstallLocation)\AppXManifest.xml"}
Get-AppXPackage -AllUsers | Foreach {Add-AppxPackage -DisableDevelopmentMode -Register "$($_.InstallLocation)\AppXManifest.xml"} ; re-register all apps, fixes an ugly issue with the Windows 10 taskbar
Get-AppxPackage *OneNote* | Remove-AppxPackage
